pymssql (2.1.4+dfsg-4) UNRELEASED; urgency=low

  [ Debian Janitor ]
  * Set field Upstream-Name in debian/copyright.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.
  * Refer to specific version of license LGPL-2.1+.
  * Update standards version to 4.5.0, no changes needed.

  [ Ondřej Nový ]
  * d/control: Update Maintainer field with new Debian Python Team
    contact address.
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

  [ Debian Janitor ]
  * Apply multi-arch hints.
    + python3-pymssql: Add Multi-Arch: same.

 -- Debian Janitor <janitor@jelmer.uk>  Sun, 19 Jul 2020 00:52:12 -0000

pymssql (2.1.4+dfsg-3) unstable; urgency=medium

  * Team upload
  [ Ondřej Nový ]
  * Bump Standards-Version to 4.4.1.

  [ Håvard Flaget Aasen ]
  * Add 0003-Remove-platform.linux_distribution.patch closes: #942791
  * Bump debhelper to 12
  * Set dpmt as maintainer closes: #930140
  * Add Rules-Requires-Root
  * Add salsa CI

 -- Håvard Flaget Aasen <haavard_aasen@yahoo.no>  Sun, 29 Dec 2019 10:28:21 +0100

pymssql (2.1.4+dfsg-2) unstable; urgency=medium

  * Team upload.

  [ Ondřej Nový ]
  * Use debhelper-compat instead of debian/compat.
  * Bump Standards-Version to 4.4.0.

  [ Andrey Rahmatullin ]
  * Drop Python 2 support.

 -- Andrey Rahmatullin <wrar@debian.org>  Sun, 11 Aug 2019 01:16:47 +0500

pymssql (2.1.4+dfsg-1) unstable; urgency=medium

  * Team upload.

  [ Ondřej Nový ]
  * d/control: Set Vcs-* to salsa.debian.org
  * d/copyright: Use https protocol in Format field
  * d/changelog: Remove trailing whitespaces
  * Convert git repository from git-dpm to gbp layout

  [ Sophie Brun ]
  * d/copyright: update excluded files
  * New upstream version 2.1.4+dfsg
  * Remove useless debian/python-pymssql.install
  * Update d/control to remove information about nagios
  * Add maintscript to remove obsolete nagios-plugin conffile
  * Update build-depends for latest upstream release: use recent freetds
    (Closes: #891670)
  * Update debian/copyright
  * Add a patch to fix spelling error
  * Add autopkgtest-pkg-python
  * Bump Standards-Version to 4.3.0
  * Add hardening build

 -- Sophie Brun <sophie@offensive-security.com>  Tue, 29 Jan 2019 11:08:20 +0100

pymssql (2.1.3+dfsg-1) unstable; urgency=medium

  * Team upload.

  [ Ondřej Nový ]
  * Fixed VCS URL (https)

  [ Geoffrey Thomas ]
  * New upstream release (Closes: #648230), with DFSG repack to avoid
    embedded freetds binaries.
    - Be compatible with newer versions of freetds (Closes: #709210).
    - Consistently respect as_dict (Closes: #590548).
    - setup.py: Don't require setuptools_git.
  * Packaging cleanups:
    - Switch from CDBS to dh sequencer, and bump d/compat to 9.
    - Build for both Python 2 and 3 using pybuild.
    - Update Standards-Version to 3.9.8 (no changes).
    - Update copyright and follow machine-readable copyright spec.
    - Switch to source format 3.0 (quilt).
    - Use uscan and Files-Excluded in debian/copyright to simplify the
      DFSG repack target, and drop debian/rules get-orig-source (just
      call `uscan --rename`).
  * Add myself to Uploaders.

 -- Geoffrey Thomas <geofft@ldpreload.com>  Wed, 24 May 2017 14:16:13 -0400

pymssql (1.0.2+dfsg-2) unstable; urgency=low

  * Team upload.

  [ Jakub Wilk ]
  * Fix watch file.
  * Use canonical URIs for Vcs-* fields.

  [ Luca Falavigna ]
  * Switch to dh_python2 (Closes: #786301).

 -- Luca Falavigna <dktrkranz@debian.org>  Sun, 30 Aug 2015 10:52:04 +0200

pymssql (1.0.2+dfsg-1) unstable; urgency=low

  [ Piotr Ożarowski ]
  * Add Debian Python Modules Team to Uploaders
  * Add Vcs-Svn and Vcs-Browser fields
  * Add debian/watch file
  * Add get-orig-source target

  [ Jan Geboers ]
  * New upstream release.
  * Standards version is 3.8.2.
  * Removed patch series, 01_nagios-plugin.patch is included upstream.
  * Removed quilt build-dependecy because of the above.

 -- Debian Python Modules Team <python-modules-team@lists.alioth.debian.org>  Sat, 11 Jul 2009 15:11:01 +0200

pymssql (1.0.1+dfsg-1) unstable; urgency=low

  * New upstream release.
  * Add quilt support; build-depend on quilt.
  * Use upstream-provided nagios plugin now (yes, it’s ours).
  * 01_nagios-plugin.patch:
    + Add an option to specify the port.
    + Fix encoding.
    + Fix SQL syntax.
    + Connect explicitly to the "master" database.
    + Improve perfdata output.

 -- Josselin Mouette <joss@debian.org>  Wed, 18 Feb 2009 12:01:32 +0100

pymssql (0.8.0+dfsg-3) unstable; urgency=low

  * check_mssql:
    + return the time value in seconds so that it integrates correctly
      with the Nagios perf_data handlers.
    + add a -P setting to define the TCP port.
  * Wrap Depends and Build-Depends.
  * Standards version is 3.8.0.

 -- Josselin Mouette <joss@debian.org>  Fri, 21 Nov 2008 14:51:41 +0100

pymssql (0.8.0+dfsg-2) unstable; urgency=low

  * Ship a Nagios plugin for MS SQL databases.

 -- Josselin Mouette <joss@debian.org>  Mon, 05 May 2008 11:50:05 +0200

pymssql (0.8.0+dfsg-1) unstable; urgency=low

  * Remove ntwdblib.dll from the upstream sources, because this file
    doesn’t have any associated source. Thanks Joerg Jaspert for
    noticing.

 -- Josselin Mouette <joss@debian.org>  Thu, 06 Mar 2008 12:49:56 +0100

pymssql (0.8.0-1) unstable; urgency=low

  * Initial release. Closes: #468175.

 -- Josselin Mouette <joss@debian.org>  Wed, 27 Feb 2008 18:49:15 +0100
